// membership.js
// This script calculates the cost of a membership.

// Function called when the form is submitted.
// Function performs the calculation and returns false.

var op = "";
var sum = 0;
var point = false;

function numPressed(num) {
	if (point) {
		theForm.display.value  = num;
		point = false;
	}
	else {
		if (theForm.display.value == "0") 
			theForm.display.value = num;
		else
			theForm.display.value += num;
		}
	theForm.secondDisplay.value += num;
}

/*function opPressed(par) {
	op = par;
	theForm.display.value += par;
}*/

function clearPressed() {

	op = "";
	sum = 0;
	theForm.display.value = "";
	theForm.secondDisplay.value = "";
}

function opPressed(opP) {

	if (point && op != "=");
	else
	{
	point = true;
	if (op == "+") 
		sum += parseFloat(theForm.display.value);
	else if (op == "-")
		sum -= parseFloat(theForm.display.value);
	else if (op == "*")
		sum *= parseFloat(theForm.display.value);
	else if (op == "/")
		sum /= parseFloat(theForm.display.value);
	else
		sum = parseFloat(theForm.display.value);
		
	theForm.display.value = sum;
	op = opP;
	
	if (op == "=")
		theForm.secondDisplay.value = "";
	else
		theForm.secondDisplay.value += opP;
    }
} // End of calculate() function.

function Dot(){

	if (point) {
		theForm.display.value = "0.";
		point = false;
	}
	else
	{
	if (theForm.display.value.indexOf(".") == -1)
		theForm.display.value += ".";
	}
	if (theForm.secondDisplay.value.indexOf(".") == -1)
		theForm.secondDisplay.value += ".";
}

function numDelete(){
var val = theForm.display.value;
var valSecond = theForm.display.value;
	if(val > 0){
	val = val.substring(0, val.length - 1);
	theForm.display.value = val;
	}
	
	if(valSecond > 0){
	valSecond = valSecond.substring(0, valSecond.length - 1);
	theForm.secondDisplay.value = valSecond;
	}
}

// Initial setup:
function init() {
    'use strict';
    document.getElementById('theForm').onsubmit = opPressed;
} // End of init() function.

window.onload = init;