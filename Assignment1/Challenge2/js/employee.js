// employee.js
// This script creates an object using form data.

// Function called when the form is submitted.
// Function creates a new object.
	var arrayAge = [];
	var countSubmit = 0; 
	var totalAvg = 0;
	var userID = parseInt((Math.random()* 1000)+1);
	var userIDArray = [];
	
function process() {
    'use strict';
	
    // Get form references:
    var userName = document.getElementById('userName').value;
    var name = document.getElementById('name').value;
	var emailAddress = document.getElementById('emailAddress').value;
    var password = document.getElementById('password').value;
    var age = document.getElementById('age').value;
    var department = document.getElementById('department').value;

	arrayAge.push(age);
	
	// Count submit
	countSubmit ++;
	
	// Define random user ID
	for (var i=0; i < userIDArray.length; i++){
	if (userID == userID) {
		userID = parseInt((Math.random()* 1000)+1);
		}
	else
		userIDArray.push(userID);
	}
		

    // Reference to where the output goes:
    var output = document.getElementById('output');
	
    // Create a new object:
	var employee = {
	    userName: userName,
	    name: name,
		emailAddress: emailAddress,
		password: password,
		age: age,
	    department: department,
	    hireDate: new Date()
	}; // Don't forget the semicolon!
	
	console.log(employee);
	
	// Create the ouptut as HTML:
    var message = '<h2>Employee Added</h2>User Name: ' + employee.userName + '<br>'; 
	message += 'Full name: ' + employee.name +  '<br>'; 
	message += 'emailAddress: ' + employee.emailAddress + '<br>'
	message += 'Age: ' +  employee.age + '<br>';
    message += 'Department: ' + employee.department + '<br>';
    message += 'Hire Date: ' + employee.hireDate.toDateString();
		
	//Push age to array and submit
	for (var i=0; i < arrayAge.length; i++){
		var	temp = parseInt(arrayAge[i])	
	}
	totalAvg += temp;

    // Display the employee object:
    output.innerHTML = message;

    // Return false:
    return false;
} // End of process() function.

function average(){
	alert('Avgerage Age: ' + totalAvg / countSubmit);
}

// Initial setup:
function init() {
    'use strict';
    document.getElementById('theForm').onsubmit = process;
} // End of init() function.
window.onload = init;
